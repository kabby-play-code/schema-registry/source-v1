package com.sample.source1;


import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableBinding(Source.class)
@EnableSchemaRegistryClient
@RestController
public class SourceV1Application {

	public static void main(String[] args) {
		SpringApplication.run(SourceV1Application.class, args);
	}

	@Autowired
	Source source;

	@PostMapping("register")
    public String register(@RequestBody Book book) {
       source.output().send(MessageBuilder.withPayload(book).build());

       System.out.printf(
               "[%s] send book, isbn = %s, title = %s, price = %d, version = %s %n" ,
               LocalDateTime.now()
               ,book.getIsbn()
               ,book.getTitle()
               ,book.getPrice()
               ,book.getVersion()
       );

       return  "OK!" + System.lineSeparator ();
   }

}
